#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.." || exit

# Temporarily copy the API specefication into the test-runner dir
cp './specification/openapi.yaml' './testing/test-runner/'

# Set the path to the Dockerfile
DOCKERFILE_PATH="${SCRIPT_DIR}/../testing/test-runner/docker-compose.yaml"

# Variable for bin/build.sh
BUILD_BACKEND="${SCRIPT_DIR}/../bin/build.sh"

# Will remove any unused containers 
docker container prune -f >/dev/null 2>&1

# Builds the docker-compose files and runs the tests for local or pipeline image
if [[ -n "$PIPELINE_IMAGE_NAME" ]]; then
    echo "Using pipeline image: $PIPELINE_IMAGE_NAME"
    export BACKEND_IMAGE_NAME="${PIPELINE_IMAGE_NAME}"
    docker-compose -f "$DOCKERFILE_PATH" build
    docker-compose -f "$DOCKERFILE_PATH" run --rm test-runner 
else
    echo "Using non-pipeline image"
    export BACKEND_IMAGE_NAME="reportingbackend"
    # This is used to build local changes
    $BUILD_BACKEND
    docker-compose -f "$DOCKERFILE_PATH" build
    docker-compose -f "$DOCKERFILE_PATH" run --rm test-runner 
fi

docker_compose_exit_code=$?

# shut down containers in docker-compose.test-runner file
docker-compose -f "$DOCKERFILE_PATH" down >/dev/null 2>&1

rm './testing/test-runner/openapi.yaml'

# This statement will check the exit code of the previous command and will report any non-zero status code
if [ ${docker_compose_exit_code} -ne 0 ]; then
  echo "Tests failed"
  docker-compose -f "$DOCKERFILE_PATH" down >/dev/null 2>&1
  exit 1
fi

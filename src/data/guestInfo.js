const Database = require("../lib/database");
const { ObjectID } = require("mongodb");

class GuestInfo{

    static async getAll() {
        const guestCollection = await getGuestCollection();
        const result = guestCollection.find().toArray();  
        return result;
    }

    static async insertGuest(data){
        const guestsCollection = await getGuestCollection();
        const  result =  await guestsCollection.insertMany(data);
        return result;
        
    }

    static async convertToISODate(Object){
        const guestCollection = await getGuestCollection();
        const result = await guestCollection.updateOne(
            {_id: ObjectID(Object._id)},
            {$set: {dateTime: new Date(Object.dateTime)}},
            { upsert: false });
        if (result.modifiedCount < 1) {
            return null;
        } else {
            const element = await guestCollection.findOne(
                {_id: ObjectID(Object._id) }
            );
            return element;
        }          
    }

    static async getGuestRecords(startDate, endDate){
        let StartDate = new Date(startDate);
        let EndDate = new Date(endDate);
        const guestCollection = await getGuestCollection();
        const result =  await guestCollection.find({"dateTime": { $gte : StartDate, $lte : EndDate}}).toArray();
        return result;
    }

    static async getHalfYearArray(startDate){
        let result;
        let StartDate = new Date(startDate);
        let year = StartDate.getFullYear();
        let month = StartDate.getMonth() + 1; 
        let date = StartDate.getDate();
        let startDateofSecondPeriod = new Date(year + '/07/01');

        const guestCollection = await getGuestCollection();
        if (month < 7){
            result =  await guestCollection.find({$expr: {
                $and:[{$eq: [{$year:"$dateTime"}, year]}, {$lte: [{$month:"$dateTime"}, month]}, {$lt: [{$dayOfMonth:"$dateTime"}, date]}]}}).toArray();
        }
        else if ((month >= 7)){
            result =  await guestCollection.find({"dateTime": { $gte : startDateofSecondPeriod, $lt : StartDate}}).toArray();

        }
        return result;
    } 
}

async function getGuestCollection(){
    const database = await Database.get();
    return database.db("guests").collection("guests");
}

module.exports = GuestInfo;

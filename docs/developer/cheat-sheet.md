# Developer Cheat Sheet

## Build

Generates a Docker image of the ReportingBackend server.

**Use this command if you do not yet have a Docker image or you have
modified files in `src`.**

```bash
bin/build.sh
```

## Up

Starts a locally running GuestInfoBackend server, along with a MongoDB
database, a RabbitMQ message queue, and networks to connect them.

This will continue to run, and generate logging messages, in your terminal
until it is terminated with Ctrl+C.

```bash
bin/up.sh
```

## Down

Removes the Docker containers and networks.

The database will be emptied of all data.

```bash
bin/down.sh
```

## Rebuild

Takes down Docker containers and networks, rebuilds the Docker image for
the server, and starts the Docker containers and networks.

<!--alex ignore simple-->
**Use this command if you have modified files in `src` and want
to restart the local server.**
If you have not modified files in `src` and simply want to restart the server,
use `bin/restart.sh` (see below).

This will continue to run, and generate logging messages, in your terminal
until it is terminated with Ctrl+C.

```bash
bin/rebuild.sh
```

## Restart

Takes down Docker containers and networks and restarts the Docker
containers and networks.

**Use this command if you have not modified files in `src` and want to
restart the local server.**
If you have modified files in `src` and want to restart the server, use
`bin/rebuild.sh` (see above).

This will continue to run, and generate logging messages, in your terminal
until it is terminated with Ctrl+C.

```bash
bin/restart.sh
```

## Test

The backend is tested locally by running:

```bash
bin/test.sh
```

This runs `bin/test.sh` which will validate the OpenAPI specification using
[Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).

These test are also run in the pipeline.

## Lint

Check all files meet standards. This command runs locally all linters that
will run in the pipeline on a push.

```bash
bin/lint.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
bin/premerge-squash.sh
```

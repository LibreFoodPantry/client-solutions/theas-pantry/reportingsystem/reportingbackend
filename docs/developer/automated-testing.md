# Automated Testing

The backend is tested locally by running:

```bash
bin/test.sh
```

This runs `bin/test.sh` which will validate the OpenAPI specification using
[Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).

These test are also run in the pipeline.

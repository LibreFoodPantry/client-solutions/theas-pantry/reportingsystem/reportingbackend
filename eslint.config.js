import globals from "globals";
import pluginJs from "@eslint/js";
import eslintPluginJsonc from 'eslint-plugin-jsonc';

export default [
  {
    env: {
      es2021: true,
      node: true,
      mocha: true
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      } 
    },

    rules: {
      "no-unused-vars": [
          "error",
          {
            "varsIgnorePattern": "should|expect"
          }
      ]
    }
  },

  pluginJs.configs.recommended,
  ...eslintPluginJsonc.configs['flat/recommended-with-json']
];

// Chai-HTTP
//   - Used to make HTTP request to the SUT (system under test).
//   - <https://www.chaijs.com/plugins/chai-http/>

const chai = require('./chai.js')

const chaiHttp = require('chai-http')
chai.use(chaiHttp)

const { SUT_BASE_URL } = require('./config.js')
const chaiRequest = chai.request(SUT_BASE_URL)

module.exports = chaiRequest

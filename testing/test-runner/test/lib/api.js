const chaiRequest = require('./chaiRequest.js');

module.exports = {
    // request for generating a report
    async getReport(startDate, endDate) {
        try {
            const response = await chaiRequest
                .get('/report')
                .query({ startDate, endDate })
            return response
        } catch (error) {
            return error.response
        }
    },
    
    // request for getting API version
    async getAPIVersion() {
        try {
            const response = await chaiRequest.get('/version')
            return response
        } catch (error) {
            return error.response
        }
    }
}

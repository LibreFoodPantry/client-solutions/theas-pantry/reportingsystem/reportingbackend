const { getAPIVersion } = require('./lib/api.js')
const yaml = require('js-yaml')
const fs = require('fs')

const openapi = yaml.safeLoad(fs.readFileSync('./lib/openapi.yaml', 'utf8'))
const expectedVersion = openapi.info.version

describe('test getAPIVersion GET /version', function () {
    it('should return the correct API version', async function () {
        const res = await getAPIVersion()
        res.should.have.status(200)

        const versionPattern = /^\d+\.\d+\.\d+$/
        res.should.have.property('text').match(versionPattern)

        res.text.should.equal(expectedVersion)
    })
})

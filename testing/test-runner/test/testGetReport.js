const chai = require('chai')
const { getReport } = require('./lib/api.js')
chai.should()

describe('test getReport GET /report', function () {
    it('should generate a report in csv format', async function () {
        const res = await getReport('2022-02-02', '2022-02-03')

        res.should.have.status(200)
        
        res.should.have.property('headers')
        res.headers['content-type'].should.include('text/csv')
    })
})


# Reporting Backend REST API Server

The idea of creating a getReport endpoint and testing code for this backend
is described in
[this file](./docs/getReport/README.md).

## How to run the code

Start the server.

```bash
docker-compose up --detach
```

If the server has previously been started, it must be stopped using

```bash
docker-compose down
```

This starts 3 containers as defined in the `docker-compose.yaml`:

- backend-database container with `mongodb` image
- rabbitmq container with `rabbitmq` image
- backend-server with `backend-server` image
  - built by the `bin/build-backend-server.sh`script
  
 -The service is now available at `http://localhost:10001/`.

You can configure the base url in `docker-compose.yaml`.

The`lib/items-api.yaml` file has the endpoints for the backend:

- `/version`GET endpoint
- returns the current backend version.
- This version number can be configured in the `src/lib/config.js`file.

- `/WCFB/report`GET endpoint
- primary endpoint of the backend to generate
- a report given the start and end dates

- Send fake data to Rabbitmq.

```bash
node ./src/send_Fakedata_RMQ/sendData.js
```

  After starting the backend server, it will require a short time for
  Rabbitmq connection. If you get error when sending fake data to Rabbitmq,
  you should wait for few more seconds and try again.

  After sending fake data, the backend server will take about 20 seconds to
  consume the data from Rabbitmq. So, the getReport endpoint should work
  after that amount of time.

  The interval, 20 sec, between each data consuming call from Rabbitmq can
  be modified in the RabbitMQ_service() method written in index.js.

- To run the test (optional)

```bash
docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run --rm test-runner
```

- Stop the server.

```bash
docker-compose down
```
